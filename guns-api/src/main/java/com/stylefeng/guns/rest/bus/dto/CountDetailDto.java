/**
 * @program school-bus
 * @description: CountDetailDto
 * @author: mf
 * @create: 2020/03/12 20:50
 */

package com.stylefeng.guns.rest.bus.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(description = "场次详情Dto")
@Data
public class CountDetailDto implements Serializable {

    @ApiModelProperty(notes = "场次id")
    private Long uuid;
    @ApiModelProperty(notes = "澡堂id")
    private Long busId;
    @ApiModelProperty(notes = "管理员姓名")
    private String driverName;
    @ApiModelProperty(notes = "澡堂状态")
    private String busStatus;
    @ApiModelProperty(notes = "洗澡开始时间")
    private String beginTime;
    @ApiModelProperty(notes = "出发日期")
    private String beginDate;
    @ApiModelProperty(notes = "已选位置")
    private String selectedSeats;
    @ApiModelProperty(notes = "位置路径")
    private String seatsNumber;
    @ApiModelProperty(notes = "场次价格")
    private Double price;

}
