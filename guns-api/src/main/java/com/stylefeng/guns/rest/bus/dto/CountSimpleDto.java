/**
 * @program school-bus
 * @description: CountSimpleDto
 * @author: mf
 * @create: 2020/03/10 01:13
 */

package com.stylefeng.guns.rest.bus.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "澡堂列表的Dto")
public class CountSimpleDto implements Serializable {
    @ApiModelProperty(notes = "场次id")
    private Long uuid;
    @ApiModelProperty(notes = "澡堂id")
    private Long busId;
    @ApiModelProperty(notes = "洗澡开始时间")
    private String beginTime;
    @ApiModelProperty(notes = "澡堂状态")
    private String busStatus;
    @ApiModelProperty(notes = "出发日期")
    private String beginDate;
    @ApiModelProperty(notes = "0:未满；1:已满")
    private String seatStatus;
}
